# Partie 1
## Sous-partie 1 : texte
Une phrase sans rien  
*Une phrase en italique*  
**Une phrase en gras**  
Un lien vers [fun-mooc.fr](www.fun-mooc.fr)  
Une ligne de `code`

## Sous-partie 2 : listes
__Liste à puce__
* item
   * sous-item
   * sous-item
* item
* item  

__Liste numérotée__
1. item
1. item
1. item
## Sous-partie 3 : code
```
# Extrait de code 
```
