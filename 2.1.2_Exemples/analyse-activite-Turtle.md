
# Objectifs 
familiarisation avec le langage Python

# Pré-requis à cette activité 
connaître les structures de bases en algorithmique et leur traduction en Python
connaitre le instructions du module Turtle
maitriser les propriétés des angles des polygones réguliers

# Durée de l'activité 
2h pour les 10 premiers exercices
Les autres sont facultatifs

# Exercices cibles
( je ne comprends pas ce que c'est)

# Description du déroulement de l'activité
Après une séance d'intro à Python, c'est le moment de l'application.
Les élèves chacun sur leurs postes réalisent les exercices dans l'ordre à leur rythme.
Tous les 10 min, un élève vient écrire une solution au tableau.
Le professeurs circule, répond aux questions et aide les élèves plus en difficultés

# Anticipation des difficultés des élèves
Exercice 3 : orientation, point de départ du tracé à donner
Exercice 4 : faire des rappels sur les angles, expliquer comment lever le crayon
Exercice 11 : le cercle n'est plus un polygone
Exercice 13 : 2 boucles imbriquées

# Gestion de l'hétérogénéïté
Les niveaux *, **, et ***
